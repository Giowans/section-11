import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';

const ResultDetails = ({result}) =>
{
    return (
        <View style = {styles.content} >
            <Image
                style = {styles.img} 
                source = {{uri: result.image_url}} 
            />
            <Text style ={styles.name}>{result.name}</Text>
            <Text style ={styles.details}>{result.rating} Stars, {result.review_count} Reviews</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    content:
    {
        margin: 15,
    },
    img:
    {
        borderRadius: 5,
        width: 250,
        height: 150,
        marginBottom: 5
    },
    name:
    {
        fontWeight: 'bold'
    },
    details:
    {
        fontStyle: 'italic'
    }
});

export default ResultDetails;