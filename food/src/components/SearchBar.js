import React from 'react';
import {View, StyleSheet, TextInput} from 'react-native';
import {Feather} from '@expo/vector-icons';

const SearchBar = ({term, onTermChange, onTermSubmited}) =>
{
    return (
        <View style = {styles.backgroundStyle}>
            <Feather name="search" style = {styles.iconStyle}/>
            <TextInput
                autoCapitalize = 'none'
                autoCorrect = {false}
                style = {styles.inputStyle}
                value = {term}
                placeholder = "Search"
                onChangeText = {onTermChange}
                onEndEditing = {onTermSubmited}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    backgroundStyle:{
        backgroundColor: '#F0EEEE',
        height: 50,
        marginHorizontal: 15,
        borderRadius: 15,
        flexDirection: 'row',
        marginTop: 10,
        marginBottom: 10
    },
    inputStyle:{
        borderWidth: 2,
        flex: 1,
        fontSize: 18,
        borderColor: 'transparent'
    },
    iconStyle:{
        fontSize: 35,
        alignSelf: 'center',
        marginHorizontal: 12
    }
});

export default SearchBar;