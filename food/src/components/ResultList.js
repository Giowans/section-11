import React from 'react';
import {View, StyleSheet, Text, FlatList, TouchableOpacity} from 'react-native'
import ResultDetails from '../components/ResultDetails';
import {withNavigation} from 'react-navigation';

const ResultList = ({title, results, navigation}) =>
{
    if(!results.length)
    {
        return null;
    }
    return (
        <View>
            <Text style = {styles.title}>{title}</Text>
            <FlatList
                showsHorizontalScrollIndicator = {false}
                horizontal
                data = {results}
                keyExtractor = {(result) => result.id}
                renderItem = {({item}) =>{
                    return(
                        <TouchableOpacity 
                            onPress = {() => navigation.navigate('SelectedResult', {id: item.id})}>
                                <ResultDetails result = {item}/>
                        </TouchableOpacity>
                    );
                }} 
            />
        </View>
    );
}

const styles = StyleSheet.create({
    title:{
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: 15
    }
});

export default withNavigation(ResultList);