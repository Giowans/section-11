import React, {useState, useEffect} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import yelp from '../api/yelp';

const useResults = () =>
{
    const [results, setResutls] =  useState([]);
    const [errMessage, setErrMessage] = useState('');
    const searchApi = async (text) =>
    {
        try
        {
            const response =  await yelp.get('/search', {
                params: {
                    limit: 50,
                    term: text,
                    location: 'san jose'
                }
            });
            setResutls(response.data.businesses);
        }catch(err){
            setErrMessage('Algo salió mal :(');
        }
    };
    useEffect(() =>{
        searchApi('tacos');
    }, []);
    return [searchApi, results, errMessage];
};

export default useResults;