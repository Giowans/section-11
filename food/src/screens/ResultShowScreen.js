import React, {useState, useEffect} from 'react';
import {View, StyleSheet, Text, FlatList, Image} from 'react-native';
import yelp from '../api/yelp';

const ResultShowScreen = ({navigation}) =>
{
    const referId = navigation.getParam('id');
    const [result, setResult] = useState(null);
    const getResult = async (id) =>
    {
        const response = await yelp.get(`/${id}`);
        setResult(response.data);
    };
    useEffect(()=>{
        getResult(referId);
    }, []);

    if(!result)
    {
        return null;
    }
    return(
        <>
            <Text>{result.name}</Text>
            <FlatList 
                data = {result.photos}
                keyExtractor = {(photo) => photo}
                renderItem = {({item})=>{
                    return(
                        <>
                            <Image
                                style = {styles.image} 
                                source = {{uri: item}}
                            />
                        </>
                    );
                }}
            />
        </>
    );
};

const styles = StyleSheet.create({
    image: 
    {
        width: 250,
        height: 150,
        borderRadius: 5,
        margin: 10
    }
});

export default ResultShowScreen;