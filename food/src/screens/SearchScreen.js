import React, {useState} from 'react';
import {View, StyleSheet, Text, ScrollView} from 'react-native';
import SearchBar from '../components/SearchBar';
import useResults from '../hooks/useResults';
import ResultList from '../components/ResultList';

const SearchScreen = () =>
{
    const [text, setText] = useState('');
    const [searchApi, results, errMessage] = useResults();
    const filterResultByPrice = (price) =>
    {
        return results.filter((result)=> {
            return result.price === price;
        });
    };
    return (
        <>
            <SearchBar 
                term = {text} 
                onTermChange = {setText}
                onTermSubmited = {() => searchApi(text)}
            />
            {errMessage ? <Text>{errMessage}</Text>: null}
            <ScrollView>
                <ResultList 
                    title='Cost Effective!'
                    results = {filterResultByPrice('$')}
                />
                <ResultList 
                    title='Bit Pricier'
                    results = {filterResultByPrice('$$')}
                />
                <ResultList 
                    title='Big Spender'
                    results = {filterResultByPrice('$$$')}
                />
            </ScrollView>
        </>
    );
};

const styles = StyleSheet.create({});

export default SearchScreen;