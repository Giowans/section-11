# Sección 11: Terminando la App del _Fetching Data_

En esta sección hemos terminado la app propuesta dentro del curso (si mal no recuerdo, a partir de la sección 9). Lo mas destacable de esta app fue el hecho de manejar los distintos datos a travéz de los componentes padres y los padres hijos.
También de manera implicita se nos muestra como podemos organizar cada componente de nuestra app y manejar una mejor jerarquía. Realmente fue una manera ideal de aplicar todo lo que hemos visto en secciones anteriores.

### Notas:
- Podemos usar los tags `<> </>` en vez de un `<View></View>` para agrupar varios componentes de _JSX_ y así no afectar el layout de la pantalla (nuestro componente _screen_).
